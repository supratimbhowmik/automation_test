package com.mercury.test;

import java.awt.AWTException;
//import java.awt.Robot;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.Assert;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class MethodRepository {
	WebDriver cDriver;
	
	public void browserAppLaunch()
	{
		// Chrome Driver Initiation.
		System.setProperty("webdriver.chrome.driver", "D:\\Automation_Testing\\Tools\\ChromeDriver\\chromedriver.exe");
		 //cDriver = new ChromeDriver();
		cDriver = new ChromeDriver();
		//cDriver.manage().deleteAllCookies();
		cDriver.manage().window().maximize();
		cDriver.get("http://www.newtours.demoaut.com/");
	}
	
	public void Login(String username, String Password) throws AWTException, IOException, InterruptedException
	{
		this.browserAppLaunch();
		
		Thread.sleep(2000);
		
		//Robot objRobot = new Robot();
		BufferedReader bReader =  new BufferedReader(new FileReader("./TestDatas.Properties"));
		
		Properties propFile = new Properties();
		propFile.load(bReader);
		//UserName
		//String userName = propFile.getProperty("Username");
		//System.out.println("Element Entry");
		Thread.sleep(2000);
		WebElement uName= cDriver.findElement(By.name("userName"));
		uName.sendKeys(username);
		
		Thread.sleep(2000);
		
		// Password
		//String pwd = propFile.getProperty("Password");
		WebElement pwdN= cDriver.findElement(By.name("password"));
		pwdN.sendKeys(Password);
		
		Thread.sleep(2000);
		
		WebElement submit= cDriver.findElement(By.name("login"));
		submit.click();
		
		this.browserAppClose();
	}
	
	public boolean verifyValidLogin(String expectedTitle,String type) throws InterruptedException
	{
		
		
		 //String expectedTitle = "Find a Flight: Mercury Tours:";
		 
		 String actualTitle = cDriver.getTitle();
		 
		if (actualTitle.equalsIgnoreCase(expectedTitle) )
		{
			//System.out.println("pass");
			return true;
		}
		else 
		{
			//System.out.println("fail");
			return false;
			
			
		}
		  
		 
		
	}

	public boolean verifyLogin(String expectedTitle) throws InterruptedException
	{
		 
		 String actualTitle = cDriver.getTitle();
		 
		if (actualTitle.equalsIgnoreCase(expectedTitle))
		{
			return true;
		} 
		else if (!actualTitle.equalsIgnoreCase(expectedTitle))
		{
			return false;
		}
		else return true;
		 
		
	}
	
	public void browserAppClose()
	{
		cDriver.close();
	}
	
	public void loginUsingExcelData(String uname, String pwd) throws InterruptedException
	{
		MethodRepository mrObj = new MethodRepository();
		mrObj.browserAppLaunch();
		Thread.sleep(2000);
		//UserName
		//String userName = propFile.getProperty("Username");
		WebElement usrName= cDriver.findElement(By.name("userName"));
		usrName.sendKeys(uname);
		
		Thread.sleep(2000);
		
		// Password
		//String pwd = propFile.getProperty("Password");
		WebElement pwdN= cDriver.findElement(By.name("password"));
		pwdN.sendKeys(pwd);
		
		Thread.sleep(2000);
		
		WebElement submit= cDriver.findElement(By.name("login"));
		submit.click();
	}
	public void excelReading() throws FileNotFoundException, InterruptedException, AWTException
	{
		//String[][] arrayexceldata = null;
		//Create an object of File class to open xlsx file
		String username,password;
		//MethodRepository mrObj = new MethodRepository();
	    File file =    new File("D:\\Automation_Testing\\Excel\\Login_Data.xls");

	    //Create an object of FileInputStream class to read excel file

	    FileInputStream inputStream = new FileInputStream(file);
		
	    try
	    {
	    Workbook wb = Workbook.getWorkbook(inputStream);
	    Sheet sh = wb.getSheet("Sheet1");
	    int rowCount = sh.getRows();
	    //int colCount = sh.getColumns();
	    //arrayexceldata = new String[rowCount-1][colCount];
	    for (int row=1;row< rowCount;row++)
	    {
	        //for(int col=0;col<colCount;col++)
	        //{
	            //arrayexceldata[row-1][col]=sh.getCell(col,row).getContents();
	        	//System.out.println(sh.getCell(col,row).getContents());
	        	username = sh.getCell(0,row).getContents();
	        	password = sh.getCell(1,row).getContents();
	        	//mrObj.loginUsingExcelData(username, password);
	        	this.Login(username,password);
	        //}
	    }

	    }
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	        e.printStackTrace();
	    } catch (BiffException e) {
	        e.printStackTrace();
	    }
	    //return arrayexceldata;
	}
}
