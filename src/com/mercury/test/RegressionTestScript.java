package com.mercury.test;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RegressionTestScript 
{
	
	@Test(enabled=false)
	public void verifiedValidLogin() throws InterruptedException, AWTException, IOException
	{
		MethodRepository mrObj = new MethodRepository();
		mrObj.browserAppLaunch();
		mrObj.Login("dasd","dasd");
		//mrObj.verifyLogin("");
		Assert.assertTrue(mrObj.verifyLogin("Find a Flight: Mercury Tours:"), "Tested"); 
		mrObj.browserAppClose();
	}
	@Test(enabled=false)
	public void verifiedInvalidLogin() throws InterruptedException, AWTException, IOException
	{
		MethodRepository mrObj = new MethodRepository();
		mrObj.browserAppLaunch();
		mrObj.Login("dasd","das");
		//mrObj.verifyLogin();
		//try{
			Assert.assertTrue(mrObj.verifyLogin("Sign-on: Mercury Tours"), "Tested"); 
		    //}
		//}catch(Exception ex){}
	
	System.out.println("End test");
		
		mrObj.browserAppClose();
	}
	
	@Test(enabled=true)
	public void excelRead() throws FileNotFoundException, InterruptedException, AWTException
	{
		MethodRepository mrObj = new MethodRepository();
		mrObj.excelReading();
	}
}
